

$('#moreWorks').on('click', function(e){

  e.preventDefault();

  var url = $(this).attr('data-url');
  var offset = $(this).attr('data-offset');
  var limit = $(this).attr('data-limit');
  $.ajax({
        url: url,
        data :{
        'offset' : offset,
        'limit'  : limit
        },
        method : 'get',
        success: function(reponsePHP){
          $('#liste').append(reponsePHP)
                     .find('ul:last-of-type')
                     .hide()
                     .slideDown();

          $('#moreWorks').attr('data-offset', offset*1 + limit*1 );
        }
      });
   });
