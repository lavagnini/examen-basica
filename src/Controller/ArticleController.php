<?php
/*
  ./src/Controller/articleController.php
*/

/**
* Contrôleur des articles
*
* @author Alexandre Lavagnini <lavagninialexandre.webdev@gmail.com>
* @copyright 2019 The PHP Group
* @version 1.0.1
*/

namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

class ArticleController extends GenericController {

    /**
  * Affichage du détail d'un article*
  * @param int $id l'id de l'article
  * @param Request $request
  * @return vue retourne la vue
  * @access public
    */

    public function showAction(int $id, Request $request){
      $article = $this->_repository->find($id);
      return $this->render('articles/show.html.twig',[
        'article'   => $article
      ]);
    }

    /**
  * Affichage de la liste des articles (secondaires)*
  * @param string $vue  nom de la vue
  * @param array[] $orderBy tableau indiquant le tri à effectuer
  * @param int $limit  détermine la limite d'affichage
  * @return vue retourne la vue
  * @access public
    */

    public function indexSecondaireAction(string $vue = 'index', array $orderBy = ['date_creation' => 'DESC'] ,int $limit = null){
      $articles = $this->_repository->findBy([], $orderBy, $limit);
      return $this->render('articles/'.$vue.'.html.twig',[
        'articles' => $articles
      ]);
    }

    /**
  * Affichage de la liste des catégories des travaux*
  * @param string $vue  nom de la vue
  * @return vue retourne la vue
  * @access public
    */

    public function indexByCategorieAction(string $vue = 'indexByCategorie', $categorie){
      $articles = $this->_repository->findByCategorie($categorie);
      return $this->render('articles/'.$vue.'.html.twig',[
        'articles' => $articles
      ]);
    }

    /**
  * Affichage de la liste des articles RSS twitter*
  * @param string $user  nom de l'utilisateur twitter
  * @return vue retourne la vue
  * @access public
    */


    public function twitterRssAction(string $user = "mvcaster", int $limit = 10 ){
          $feedIo = \FeedIo\Factory::create()->getFeedIo();
          $url = 'http://twitrss.me/twitter_user_to_rss/?user=' . $user;
          try {
            $request = $feedIo->read($url)->getFeed();
          } catch (\Exception $e) {
            return $this->render('articles/erreur.html.twig');
          }
          return $this->render('articles/twitterRss.html.twig',[
            'rss' => $request,
            'limit' => $limit
          ]);
        }



        /**
      * Affichage de la liste des articles avec pagination*
      * @param string $vue  nom de la vue
      * @param array[] $orderBy tableau indiquant le tri à effectuer
      * @param int $limit  détermine la limite d'affichage
      * @return vue retourne la vue
      * @access public
        */

      public function indexAction(string $vue = 'index', Request $request, PaginatorInterface $paginator){
           $request = Request::createFromGlobals();
           $query = $this->_repository->findAllQuery();
           $pagination = $paginator->paginate(
                $query, /* query NOT result */
                $request->query->getInt('page', 1), /* page number */
                4 /* limit per page*/
            );
           return $this->render('articles/'.$vue.'.html.twig',[
             'articles' => $pagination
           ]);
          }
}
