<?php
/*
  ./src/Controller/categorieController.php
*/

/**
* Contrôleur des categories
*
* @author Alexandre Lavagnini <lavagninialexandre.webdev@gmail.com>
* @copyright 2019 The PHP Group
* @version 1.0.1
*/

namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Categorie;
use Symfony\Component\HttpFoundation\Request;

class CategorieController extends GenericController {

    /**
  * Affichage du détail d'une categorie*
  * @param int $id l'id de la categorie
  * @param Request $request
  * @return vue retourne la vue
  * @access public
    */

    public function showAction(int $id, Request $request){
      $categorie = $this->_repository->find($id);
      return $this->render('categories/show.html.twig',[
        'categorie'   => $categorie
      ]);
    }

    /**
  * Affichage de la liste des catégories*
  * @param string $vue  nom de la vue
  * @param array[] $orderBy tableau indiquant le tri à effectuer
  * @param int $limit  détermine la limite d'affichage
  * @return vue retourne la vue
  * @access public
    */

    public function indexAction(string $vue = 'index', int $limit = null){
      $categories = $this->_repository->findBy([], null,  $limit);
      return $this->render('categories/'.$vue.'.html.twig',[
        'categories' => $categories
      ]);
    }


}
