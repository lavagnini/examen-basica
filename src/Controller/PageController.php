<?php
/*
  ./src/Controller/PageController.php
*/

/**
* Contrôleur des pages
*
* @author Alexandre Lavagnini <lavagninialexandre.webdev@gmail.com>
* @copyright 2019 The PHP Group
* @version 1.0.1
*/

namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Page;
use Symfony\Component\HttpFoundation\Request;

class PageController extends GenericController {

    /**
  * Affichage du détail d'une page*
  * @param int $id l'id de la page
  * @param Request $request
  * @return vue retourne la vue
  * @access public
    */

    public function showAction(int $id, Request $request){
      $page = $this->_repository->find($id);
      return $this->render('pages/show.html.twig',[
        'page'   => $page
      ]);
    }

      /**
  * Affichage de la liste des pages*
  * @param int $currentId  Id de la page actuelle
  * @return vue retourne la vue
  * @access public
  */

  public function indexAction(int $currentId = null){
    $pages = $this->_repository->findAll();
    return $this->render('pages/index.html.twig',[
      'pages' => $pages,
      'currentId' => $currentId
    ]);
  }


}
