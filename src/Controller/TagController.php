<?php
/*
  ./src/Controller/tagController.php
*/

/**
* Contrôleur des tags
*
* @author Alexandre Lavagnini <lavagninialexandre.webdev@gmail.com>
* @copyright 2019 The PHP Group
* @version 1.0.1
*/

namespace App\Controller;
use Ieps\Core\GenericController;
use App\Entity\Tag;
use Symfony\Component\HttpFoundation\Request;

class TagController extends GenericController {

    /**
  * Affichage du détail d'un tag*
  * @param int $id l'id du tag
  * @param Request $request
  * @return vue retourne la vue
  * @access public
    */

    public function showAction(int $id, Request $request){
      $tag = $this->_repository->find($id);
      return $this->render('tags/show.html.twig',[
        'tag'   => $tag
      ]);
    }



}
