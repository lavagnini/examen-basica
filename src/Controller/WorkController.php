<?php
/*
  ./src/Controller/WorkController.php
*/

/**
* Contrôleur des travaux
*
* @author Alexandre Lavagnini <lavagninialexandre.webdev@gmail.com>
* @copyright 2019 The PHP Group
* @version 1.0.1
*/

namespace App\Controller;
use Ieps\Core\GenericController;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Work;

class WorkController extends GenericController {

  /**
* Affichage de la liste des travaux*
* @param string $vue  nom de la vue
* @param array[] $orderBy tableau indiquant le tri à effectuer
* @param int $limit  détermine la limite d'affichage
* @return vue retourne la vue
* @access public
  */

  public function indexAction(string $vue = 'index', array $orderBy = ['date_creation' => 'DESC'] ,int $limit = null){
    $works = $this->_repository->findBy([], $orderBy, $limit);
    return $this->render('works/'.$vue.'.html.twig',[
      'works' => $works
    ]);
  }


      /**
    * Affichage du détail d'une travail*
    * @param int $id l'id de la page
    * @param Request $request
    * @return vue retourne la vue
    * @access public
      */

      public function showAction(int $id, Request $request){
        $work = $this->_repository->find($id);
        return $this->render('works/show.html.twig',[
          'work'   => $work
        ]);
      }

      /**
    * Requête Ajax*
    * @param string $vue  nom de la vue
    * @param array[] $orderBy tableau indiquant le tri à effectuer
    * @param int $limit  détermine la limite d'affichage
    * @return vue retourne la vue
    * @access public
      */

      public function ajaxAction(string $vue = 'liste', array $orderBy = ['date_creation' => 'DESC'], int $limit = null, Request $request){
        $offset = $request->query->get('offset');
        $limit = $request->query->get('limit');
        $works = $this->_repository->findBy([], $orderBy, $limit, $offset);
        return $this->render('works/'.$vue.'.html.twig',[
          'works' => $works
        ]);
      }

      /**
    * Affichage de la liste des travaux*
    * @param string $vue  nom de la vue
    * @param array[] $orderBy tableau indiquant le tri à effectuer
    * @param int $limit  détermine la limite d'affichage
    * @return vue retourne la vue
    * @access public
      */

      public function similaireAction($tags = null, int $limit = null){
        $works_array = $this->_repository->findByTags($tags);
        return $this->render('works/similaire.html.twig',[
          'works_array' => $works_array,
          'limit' => $limit
        ]);
      }


    }
