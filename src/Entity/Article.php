<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var int $id id de l'article'
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string $titrefr  titre de l'article (français)
     */
    private $titrefr;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string $titreen  titre de l'article (anglais)
     */
    private $titreen;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string $slugfr  slug de l'article (français)
     */
    private $slugfr;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string $slugen  slug de l'article (français)
     */
    private $slugen;

    /**
     * @ORM\Column(type="datetime")
     * @var datetime date de création de l'article'
     */
    private $date_creation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string $image  Image de l'article
     */
    private $image;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string $textefr  Texte de l'article (français)
     */
    private $textefr;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string $texteen  Texte de l'article (anglais)
     */
    private $texteen;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Categorie", inversedBy="articles")
     * @var string $categories  Catégories à laquelle appartient l'article
     */
    private $categories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->date_creation = new \Datetime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitrefr(): ?string
    {
        return $this->titrefr;
    }

    public function setTitrefr(string $titrefr): self
    {
        $this->titrefr = $titrefr;

        return $this;
    }

    public function getTitreen(): ?string
    {
        return $this->titreen;
    }

    public function setTitreen(string $titreen): self
    {
        $this->titreen = $titreen;

        return $this;
    }

    public function getSlugfr(): ?string
    {
        return $this->slugfr;
    }

    public function setSlugfr(string $slugfr): self
    {
        $this->slugfr = $slugfr;

        return $this;
    }

    public function getSlugen(): ?string
    {
        return $this->slugen;
    }

    public function setSlugen(string $slugen): self
    {
        $this->slugen = $slugen;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->date_creation;
    }

    public function setDateCreation(\DateTimeInterface $date_creation): self
    {
        $this->date_creation = $date_creation;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getTextefr(): ?string
    {
        return $this->textefr;
    }

    public function setTextefr(?string $textefr): self
    {
        $this->textefr = $textefr;

        return $this;
    }

    public function getTexteen(): ?string
    {
        return $this->texteen;
    }

    public function setTexteen(?string $texteen): self
    {
        $this->texteen = $texteen;

        return $this;
    }

    /**
     * @return Collection|Categorie[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Categorie $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Categorie $category): self
    {
        if ($this->categories->contains($category)) {
            $this->categories->removeElement($category);
        }

        return $this;
    }

    public function __toString()
    {
      return $this->titrefr;
    }
}
