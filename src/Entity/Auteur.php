<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AuteurRepository")
 */
class Auteur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var int $id id de l'auteur
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string $nom  Nom de l'auteur
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string $prenom   Prenom de l'auteur
     */
    private $prenom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Work", mappedBy="auteur")
     * @var string $works  Travaux de l'auteur
     */
    private $works;

    public function __construct()
    {
        $this->works = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return Collection|Work[]
     */
    public function getWorks(): Collection
    {
        return $this->works;
    }

    public function addWork(Work $work): self
    {
        if (!$this->works->contains($work)) {
            $this->works[] = $work;
            $work->setAuteur($this);
        }

        return $this;
    }

    public function removeWork(Work $work): self
    {
        if ($this->works->contains($work)) {
            $this->works->removeElement($work);
            // set the owning side to null (unless already changed)
            if ($work->getAuteur() === $this) {
                $work->setAuteur(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
      return $this->nom;
    }
}
