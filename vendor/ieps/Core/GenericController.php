<?php
/*
    ./vendor/ieps/Core/GenericController.php
 */
namespace Ieps\Core;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\HttpFoundation\Request;

class GenericController extends AbstractController {

      protected $_repository;
      // Nom du repository [Ex: \App\Repository\PostRepository]
      protected $_className;
      // Nom de l'enregistrement [ex: post]
      protected $_r;

      public function __construct(RegistryInterface $registry){
        // static::class est du genre 'App\Controller\PostController'
        // Je veux garder juste 'Post'
        $name = str_replace('App\\\\', '', str_replace('Controller', '', static::class));
        // Je construis '\App\Repository\PostRepository'
        $this->_className = '\App\Repository\\' . $name. 'Repository';
        // Je construis 'post'
        $this->_r = strtolower($name);

        // Je crée un nouveau \App\Repository\PostRepository
        $this->_repository = new $this->_className($registry, '\App\Entity\\'.$name);
      }

      public function showAction(int $id, Request $request){
        // Je vais chercher l'enregistrement
        $r= $this->_repository->find($id);
        return $this->render($this->_r . 's/show.html.twig',[
          $this->_r => $r
        ]);
      }

}
